﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BlazorAuthentication.Api.Controllers
{
    [ApiController]
    [Route("api/secret")]
    [Authorize]
    public class SecretController : ControllerBase
    {
        public string Index()
        {
            return "Hi! Secret message from Api";
        }
    }
}
